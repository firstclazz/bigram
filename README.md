# README #

Implementation of algorithm for calculation and printing a histogram of bigrams
calculated from various text sources (like files, static text, http, etc.)

### Motivation ###

The main goal of this example is to present a solution for analyzing and processing
extensive text with a suitable time and space complexity as well as illustration 
of structure design, which is easily expandable for various types of text sources
and languages. 

### Problem analysis ###

A bigram or digram is a sequence of two adjacent elements from a string of tokens,
which are typically letters, syllables or words. A bigram is an n-gram for n=2.
The frequency distribution of every bigram in a string is commonly used for simple
statistical analysis of text in many applications, including computational linguistics,
cryptography, speech recognition and so on.

The histogram of bigram is a frequency (count of occurrences) for every bigram
found during the text analysis.

#### Problem complexity ####

    Let "n" be a number of words in analysed text
    Let "w" be a number of unique words defined in a particular language
    Let "fUniqCnt(n)" be a function which returns the number of unique words
    Let "fBigramCnt(n)" be a function which returns the number of all bigrams in analysed text

Given that:
  
    fUniqCnt(n) <= n <= w
    fBigramCnt(n) = n! - n <= w! - w

#### Space complexity ####

The space complexity of this problem is not possible to specify for the whole interval. But it is true that:

    space complexity of storing bigrams is factorial for interval <0, w! - w> = O(n!)
    space complexity of storing bigrams is constant for interval (w! - w, infinity) = O(1)

#### Time complexity ####

The optional time complexity for this problem is:

    O(n)

#### Summary ####

To solve the problem for an English text using this approach we would need to allocate
memory for (160.000! - 160.000) bigrams, since English language is defined by 
approximately 160.000 words. This is not suitable for our algorithm and we will need to 
design a more suitable structure to store the histogram.   

### Requirements ###

* Design algorithm which has linear time complexity or better
* Design algorithm which has better than factorial space complexity
* Design solution structure which is easy to extend to handle any type of text source  
* Design solution structure which is easy to extend to any language type
* Implement algorithm for analysing text from file and calculating bigram histogram
* Implement algorithm for printing the histogram to console

### Solution description ###

The current implementation use special structure to store histogram data.
This structure is based on HashMap and nested maps. The key of the first map contains
all unique words from analysed text (first word of bigram), the value of this map contains
nested HashMap which persists the second word of bigram as a key and bigram frequency as a value.
Space complexity for this structure is:
    
    O(fUniqCnt(n)^2) with maximum w^2

Algorithm reads and tokenizes (word detection) the data using streams which significantly reduce
memory requirements for processing data. All data are processed in one iteration with
time complexities: 
    
    Calculating histogram: O(n)
    Getting frequency of particular bigram: O(1)
    Printing histogram: O(n^2)

There are several interfaces for histogram structure, text readers and word tokenizer defined in 
the solution. This ensures the extension of solution to be able to read from a new type of source 
or to adapt to a new language type easily. All interfaces using input sources implement
Closeable interface, so we can call them using try-with-resource keyword to avoid memory leaks.

Current implementation contains two text readers:

* string stream reader (StringTextReader)
* file stream reader (FileTextReader)

and one generic implementation of language tokenizer based on latin (LatinWordTokenizer)

#### Structure ####

* Application: main class which triggers bigram analysis and handles input parameters
* Bigram: entry point to bigram algorithm. The class contains references to text
reader and histogram implementation according to requirements
* Histogram: interface of internal bigram histogram data structure
* HashMapHistogram: implementation of internal bigram histogram data structure
based on nested HashMaps
* WordTokenizer: interface for word tokenizer implementation of specific language
* LatinWordTokenizer: default implementation of word tokenizer for latin based languages 
* TextReader: interface for implementation of reading text from several types of sources
* StringTextReader: implementation of stream reader from static string
* FileTextReader: implementation of stream reader from given file

### How to build solution ###

#### Requirement ####
* Java 1.8 or newer

#### Mac OS, Linux users ####

    ./gradlew clean build

#### Windows users ####
    
    gradlew.bat clean build

### How to run solution ###

from static text

    java -jar build/libs/bigram-1.0-SNAPSHOT.jar -t "The quick brown fox and the quick blue hare"

from file
    
    java -jar build/libs/bigram-1.0-SNAPSHOT.jar -f src/test/resources/bigram.txt