package com.firstclazz.bigram.impl;

import com.firstclazz.bigram.TextReader;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class FileTextReaderTest {

    @Test
    void testCreateTextReaderFileNotFound() {

        assertThrows(FileNotFoundException.class, () -> new FileTextReader("src/test/resources/wrong-file.txt"));
    }

    @Test
    void testCreateInputStream() throws IOException {

        try (TextReader reader = new FileTextReader("src/test/resources/bigram.txt")) {

            assertNotNull(reader);
            assertEquals('T', reader.getReader().read());
        }
    }
}