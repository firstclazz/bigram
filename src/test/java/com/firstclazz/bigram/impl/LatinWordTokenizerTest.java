package com.firstclazz.bigram.impl;

import com.firstclazz.bigram.TextReader;
import com.firstclazz.bigram.WordTokenizer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class LatinWordTokenizerTest {

    @Mock
    private TextReader reader;

    @Test
    public void testCreateTokenizerWithoutReader() {

        assertThrows(IllegalArgumentException.class, () -> new LatinWordTokenizer(null));
    }

    @Test
    public void testGetNextTokenFromEmptySource() throws IOException {

        Mockito.when(reader.getReader())
                .thenReturn(new InputStreamReader(new ByteArrayInputStream("".getBytes())));

        WordTokenizer tokenizer = new LatinWordTokenizer(reader);
        assertNull(tokenizer.getNextToken());
    }

    @Test
    public void testGetNextToken() throws IOException {

        Mockito.when(reader.getReader())
                .thenReturn(new InputStreamReader(
                        new ByteArrayInputStream("(The) quick\tbrown\nfox\t\nand, the - quick? blue! hare. co-operate ! ? \" ' () + - * / 345".getBytes())));

        WordTokenizer tokenizer = new LatinWordTokenizer(reader);
        assertEquals("the", tokenizer.getNextToken());
        assertEquals("quick", tokenizer.getNextToken());
        assertEquals("brown", tokenizer.getNextToken());
        assertEquals("fox", tokenizer.getNextToken());
        assertEquals("and", tokenizer.getNextToken());
        assertEquals("the", tokenizer.getNextToken());
        assertEquals("quick", tokenizer.getNextToken());
        assertEquals("blue", tokenizer.getNextToken());
        assertEquals("hare", tokenizer.getNextToken());
        assertEquals("co-operate", tokenizer.getNextToken());
        assertNull(tokenizer.getNextToken());
    }

    @Test
    public void testGetNextTokenWithDelimitersOnly() throws IOException {

        Mockito.when(reader.getReader())
                .thenReturn(new InputStreamReader(new ByteArrayInputStream(".?!-'\": . ! ".getBytes())));

        WordTokenizer tokenizer = new LatinWordTokenizer(reader);
        assertNull(tokenizer.getNextToken());

    }
}