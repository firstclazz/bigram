package com.firstclazz.bigram.impl;

import com.firstclazz.bigram.TextReader;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class StringTextReaderTest {

    @Test
    void testCreateStringTextReaderWithNullString() {

        assertThrows(IllegalArgumentException.class, () -> new StringTextReader(null));
    }

    @Test
    public void testNextToken() throws IOException {

        try (TextReader reader = new StringTextReader("The quick brown fox and the quick blue hare.")) {

            assertNotNull(reader);
            assertEquals('T', reader.getReader().read());
        }
    }
}