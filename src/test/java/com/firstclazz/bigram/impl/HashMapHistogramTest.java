package com.firstclazz.bigram.impl;

import com.firstclazz.bigram.Histogram;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HashMapHistogramTest {

    @Test
    public void testAddBigramWrongParams() {

        Histogram histogram = new HashMapHistogram();
        assertThrows(IllegalArgumentException.class, () -> histogram.addBigram(null, null));
        assertThrows(IllegalArgumentException.class, () -> histogram.addBigram("firstWord", null));
        assertThrows(IllegalArgumentException.class, () -> histogram.addBigram(null, "secondWord"));
        assertThrows(IllegalArgumentException.class, () -> histogram.addBigram("firstWord", " "));
        assertThrows(IllegalArgumentException.class, () -> histogram.addBigram("", "secondWord"));
    }

    @Test
    public void testGetFrequencyWrongParams() {

        Histogram histogram = new HashMapHistogram();
        assertThrows(IllegalArgumentException.class, () -> histogram.getFrequency(null, null));
        assertThrows(IllegalArgumentException.class, () -> histogram.getFrequency("firstWord", null));
        assertThrows(IllegalArgumentException.class, () -> histogram.getFrequency(null, "secondWord"));
    }

    @Test
    public void testHistogramForExistingKeys() {

        Histogram histogram = new HashMapHistogram();
        histogram.addBigram("the", "quick");
        histogram.addBigram("quick", "brown");
        histogram.addBigram("brown", "fox");
        histogram.addBigram("fox", "and");
        histogram.addBigram("and", "the");
        histogram.addBigram("the", "quick");
        histogram.addBigram("quick", "blue");
        histogram.addBigram("blue", "hare");

        assertEquals(Integer.valueOf(2), histogram.getFrequency("the", "quick"));
        assertEquals(Integer.valueOf(1), histogram.getFrequency("quick", "brown"));
        assertEquals(Integer.valueOf(1), histogram.getFrequency("brown", "fox"));
        assertEquals(Integer.valueOf(1), histogram.getFrequency("fox", "and"));
        assertEquals(Integer.valueOf(1), histogram.getFrequency("and", "the"));
        assertEquals(Integer.valueOf(1), histogram.getFrequency("quick", "blue"));
        assertEquals(Integer.valueOf(1), histogram.getFrequency("blue", "hare"));
    }

    @Test
    public void testHistogramForNonExistingKeys() {

        Histogram histogram = new HashMapHistogram();
        histogram.addBigram("the", "quick");

        assertEquals(Integer.valueOf(0), histogram.getFrequency("the", "shadow"));
        assertEquals(Integer.valueOf(0), histogram.getFrequency("alert", "warning"));
    }
}