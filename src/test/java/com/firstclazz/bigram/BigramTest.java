package com.firstclazz.bigram;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class BigramTest {

    @Mock
    private WordTokenizer wordTokenizer;

    @Test
    public void testInstantiation() {

        // Missing WordTokenizer implementation
        assertThrows(IllegalArgumentException.class, () -> new Bigram(null));

        // Correct arguments
        new Bigram(wordTokenizer);
    }

    @Test
    public void testCalculateHistogram() throws IOException {

        Bigram bigram = new Bigram(wordTokenizer);
        Histogram histogram = bigram.calculateHistogram();
        assertNotNull(histogram);
    }
}