package com.firstclazz.bigram;

import com.firstclazz.bigram.impl.FileTextReader;
import com.firstclazz.bigram.impl.LatinWordTokenizer;
import com.firstclazz.bigram.impl.StringTextReader;
import org.apache.commons.cli.*;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * The main Application implementation which triggers calculation of bigrams histogram process.
 * This class also handle application input arguments and provide output into console.
 *
 * @author Michal Bogusky
 */
public class Application {

    /**
     * Main class of Application.
     *
     * @param args input arguments
     */
    public static void main(final String[] args) {

        // configure commandline CLI
        Options options = new Options();
        options.addOption("t", "text", true, "print histogram of bigram from the input text");
        options.addOption("f", "file", true, "print histogram of bigram from the input file");

        // parse commandline argumets
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd;
        try {
            cmd = parser.parse(options, args);
            if (cmd.hasOption("t") && cmd.hasOption("f")) {

                throw new ParseException("Choose only one option -f or -t");
            }

            // instantiate suitable text reader according to input arguments
            TextReader textReader;
            if (cmd.hasOption("t")) {

                // create text reader from string
                textReader = new StringTextReader(cmd.getOptionValue("t"));
            } else if (cmd.hasOption("f")) {

                // create text reader from file
                try {

                    textReader = new FileTextReader(cmd.getOptionValue("f"));
                } catch (FileNotFoundException e) {

                    throw new ParseException("Input file doesn't exists!");
                }
            } else {

                throw new ParseException("Choose one option -f or -t");
            }

            // create latin word tokenizer which reads words from text reader
            WordTokenizer wordTokenizer = new LatinWordTokenizer(textReader);

            // calculate and print histogram of bigrams
            try (Bigram bigram = new Bigram(wordTokenizer)) {

                Histogram histogram = bigram.calculateHistogram();
                histogram.printHistogram();
            } catch (IOException e) {

                System.out.println("Error while reading input stream!");
                System.exit(1);
            } catch (Exception e) {

                System.out.println("Ups, something went wrong!");
                System.exit(1);
            }

        } catch (ParseException e) {

            // print help
            System.out.println("Error parsing command-line arguments!");
            System.out.println(e.getMessage());
            System.out.println("Please, follow the instructions below:\n");
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("\njava -jar BigramApp.jar <--file=<path>>"
                    + "\njava -jar BigramApp.jar <--text=<text>>\n\nArguments:\n", options);
            System.exit(1);
        }
    }
}
