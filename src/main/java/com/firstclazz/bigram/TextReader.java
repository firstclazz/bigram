package com.firstclazz.bigram;

import java.io.Closeable;
import java.io.InputStreamReader;

/**
 * Interface of source text readers.
 */
public interface TextReader extends Closeable {

    /**
     * Return input stream for analysed text.
     *
     * @return input stream
     */
    InputStreamReader getReader();
}
