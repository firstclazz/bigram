package com.firstclazz.bigram;

/**
 * Interface of bigram histogram data structure.
 *
 * @author Michal Bogusky
 */
public interface Histogram {

    /**
     * Add bigram into the structure and recalculate count.
     *
     * @param firstWord  first word of bigram
     * @param secondWord second word of bigram
     * @return current count of bigram
     */
    Integer addBigram(String firstWord, String secondWord);

    /**
     * Get number of frequency of particular bigram.
     *
     * @param firstWord  first word of bigram
     * @param secondWord second word of bigram
     * @return nymber of bigram frequency
     */
    Integer getFrequency(String firstWord, String secondWord);

    /**
     * Print bigram histogram into the console.
     */
    void printHistogram();
}
