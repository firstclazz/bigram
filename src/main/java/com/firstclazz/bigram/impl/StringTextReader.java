package com.firstclazz.bigram.impl;

import com.firstclazz.bigram.TextReader;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Implementation of text reader which read data from the static text.
 *
 * @author Michal Bogusky
 */

public class StringTextReader implements TextReader {

    /**
     * Input stream reader.
     */
    private final InputStreamReader reader;

    /**
     * Constructor.
     *
     * @param source static source text
     */
    public StringTextReader(final String source) {

        if (source == null) {
            throw new IllegalArgumentException("Input source text cannot be null!");
        }
        this.reader = new InputStreamReader(new ByteArrayInputStream(source.getBytes()));
    }

    /**
     * Return reference to initialised bytearray input stream.
     *
     * @return reference to initialised bytearray input stream
     */
    @Override
    public InputStreamReader getReader() {
        return reader;
    }

    @Override
    public void close() throws IOException {

        if (reader != null) {
            reader.close();
        }
    }
}
