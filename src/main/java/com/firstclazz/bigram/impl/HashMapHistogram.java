package com.firstclazz.bigram.impl;

import com.firstclazz.bigram.Histogram;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Internal implementation of bigram histogram data structure based on nested HashMaps.
 *
 * @author Michal Bogusky
 */
public class HashMapHistogram implements Histogram {

    /**
     * Internal data structure for storing bigram histogram.
     * <p>
     * Key: first word of bigram
     * Value: nested HashMap which contains:
     * Key: second word of bigram;
     * Value: number of occurrences
     */
    private Map<String, Map<String, Integer>> histogram = new HashMap<>();

    /**
     * Add bigram into internal histogram representation and update number of occurrences.
     *s
     * @param firstWord  first word of bigram
     * @param secondWord second word of bigram
     * @return return current number of bigram occurrences
     */
    @Override
    public Integer addBigram(String firstWord, String secondWord) {

        if (StringUtils.isBlank(firstWord) || StringUtils.isBlank(secondWord)) {
            throw new IllegalArgumentException("Both words cannot be null!");
        }

        Map<String, Integer> secondWordMap = histogram.computeIfAbsent(firstWord, k -> new HashMap<>());

        int count = secondWordMap.computeIfAbsent(secondWord, k -> 0);
        secondWordMap.put(secondWord, count + 1);

        return count + 1;
    }

    /**
     * Get number of occurrences of particular bigram.
     *
     * @param firstWord  first word of bigram
     * @param secondWord second word of bigram
     * @return return number of bigram occurrences
     */
    @Override
    public Integer getFrequency(String firstWord, String secondWord) {

        if (StringUtils.isBlank(firstWord) || StringUtils.isBlank(secondWord)) {
            throw new IllegalArgumentException("Both words cannot be null!");
        }

        Map<String, Integer> secondWordMap = histogram.get(firstWord);
        if (secondWordMap == null) {
            return 0;
        }

        Integer count = secondWordMap.get(secondWord);

        return (count == null) ? 0 : count;
    }

    /**
     * Print bigram histogram into the console.
     */
    @Override
    public void printHistogram() {

        histogram.forEach((k1, v1) -> v1.forEach((k2, v2) -> {
            System.out.println(k1 + " " + k2 + " " + v2);
        }));
    }
}
