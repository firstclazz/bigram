package com.firstclazz.bigram.impl;

import com.firstclazz.bigram.TextReader;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 * Implementation of text reader which read data from the input file.
 *
 * @author Michal Bogusky
 */
public class FileTextReader implements TextReader {

    /**
     * Input stream reader.
     */
    private final InputStreamReader reader;

    /**
     * Constructor.
     *
     * @param source path to source file
     * @throws FileNotFoundException in case of file doesn't exist
     */
    public FileTextReader(final String source) throws FileNotFoundException {

        this.reader = new InputStreamReader(new FileInputStream(source));
    }

    /**
     * Return reference to initialised file input stream.
     *
     * @return reference to initialised file input stream
     */
    @Override
    public InputStreamReader getReader() {
        return reader;
    }

    @Override
    public void close() throws IOException {

        reader.close();
    }
}
