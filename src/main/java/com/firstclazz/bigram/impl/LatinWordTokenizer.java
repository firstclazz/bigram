package com.firstclazz.bigram.impl;

import com.firstclazz.bigram.TextReader;
import com.firstclazz.bigram.WordTokenizer;

import java.io.IOException;
import java.io.StreamTokenizer;

/**
 * Implementation of the word tokenizer for languages based on latin.
 *
 * @author Michal Bogusky
 */
public class LatinWordTokenizer implements WordTokenizer {

    /**
     * Reference to text reader
     */
    final private TextReader reader;

    /**
     * Reference to configured stream tokenizer for specific language type.
     */
    final private StreamTokenizer tokenizer;

    /**
     * Constructor. Instantiate stream tokenizer and provides configuration for specific language type.
     *
     * @param reader text reader
     */
    public LatinWordTokenizer(final TextReader reader) {

        if (reader == null) {
            throw new IllegalArgumentException("Cannot instantiate LatinWordTokenizer without input reader!");
        }
        this.reader = reader;

        this.tokenizer = new StreamTokenizer(reader.getReader());
        tokenizer.lowerCaseMode(true);
        tokenizer.eolIsSignificant(true);
        tokenizer.whitespaceChars(0, '@');
        tokenizer.wordChars('-', '-');
    }

    /**
     * Return next word from stream.
     *
     * @return next available word. null otherwise
     * @throws IOException in case of problem with reading from input source
     */
    @Override
    public String getNextToken() throws IOException {

        while (tokenizer.nextToken() != StreamTokenizer.TT_EOF && tokenizer.sval == null) {

            if (tokenizer.ttype == StreamTokenizer.TT_EOF) {
                return null;
            }
        }

        return tokenizer.sval;
    }


    @Override
    public void close() throws IOException {

        if (reader != null) {
            reader.close();
        }
    }
}
