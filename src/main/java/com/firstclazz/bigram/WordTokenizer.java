package com.firstclazz.bigram;

import java.io.Closeable;
import java.io.IOException;

/**
 * Interface of word tokenizer.
 *
 * @author Michal Bogusky
 */
public interface WordTokenizer extends Closeable {

    /**
     * Return next word from the input stream based on language group.
     *
     * @return word or null in case when no other words are available.
     * @throws IOException in case of problems with reading from stream
     */
    String getNextToken() throws IOException;
}
