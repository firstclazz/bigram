package com.firstclazz.bigram;

import com.firstclazz.bigram.impl.HashMapHistogram;

import java.io.Closeable;
import java.io.IOException;

/**
 * Entry point to bigram algorithm. The class contains references to text
 * reader word tokenizer and histogram implementation according to requirements.
 *
 * @author Michal Bogusky
 */
public class Bigram implements Closeable {

    /**
     * reference to word word tokenizer implementation
     */
    final private WordTokenizer wordTokenizer;

    /**
     * reference to internal structure of bigram histogram
     */
    final private Histogram histogram = new HashMapHistogram();

    /**
     * Constructor.
     *
     * @param wordTokenizer word tokenizer implementation
     */
    public Bigram(final WordTokenizer wordTokenizer) {

        if (wordTokenizer == null) {
            throw new IllegalArgumentException("The WordTokenizer input parameters cannot be null!");
        }

        this.wordTokenizer = wordTokenizer;
    }

    /**
     * Calculate the histogram from analyzed text.
     *
     * @return histogram of bigrams
     * @throws IOException in case of problem with reading from input stream
     */
    public Histogram calculateHistogram() throws IOException {

        String firstWord = wordTokenizer.getNextToken();
        if (firstWord == null) {
            return histogram;
        }
        String secondWord;

        while ((secondWord = wordTokenizer.getNextToken()) != null) {

            histogram.addBigram(firstWord, secondWord);
            firstWord = secondWord;
        }

        return histogram;
    }

    @Override
    public void close() throws IOException {

            wordTokenizer.close();
    }
}
